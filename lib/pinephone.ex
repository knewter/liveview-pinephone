defmodule Pinephone do
  defmodule LED do
    @on "1"
    @off "0"
    @led_file "/sys/devices/platform/led-controller/leds/white:flash/brightness"

    def turn_on(), do: write_led(@on)
    def turn_off(), do: write_led(@off)

    def toggle() do
      if is_on?() do
        turn_off()
      else
        turn_on()
      end
    end

    def is_on?() do
      String.trim(file_contents()) == @on
    end

    defp file_contents() do
      case File.read(@led_file) do
        {:ok, contents} -> contents
        _ -> ""
      end
    end

    defp write_led(val) do
      File.write(@led_file, val)
    end
  end
  def toggle_flashlight, do: LED.toggle()

  defmodule Server do
    defmodule Hardware do
      defstruct led: false

      def read_state() do
        %__MODULE__{
          led: Pinephone.LED.is_on?()
        }
      end
    end

    use GenServer

    # Client

    def start_link([]) do
      GenServer.start_link(__MODULE__, [], name: __MODULE__)
    end

    def get_state(), do: GenServer.call(__MODULE__, :get_state)
    def toggle_led(), do: GenServer.call(__MODULE__, :toggle_led)

    # Server (callbacks)

    @impl true
    def init([]) do
      {:ok, Hardware.read_state()}
    end

    @impl true
    def handle_call(:get_state, _, state) do
      {:reply, state, state}
    end
    def handle_call(:toggle_led, _, state) do
      Pinephone.LED.toggle()
      new_state = Hardware.read_state()
      LiveUpdates.notify_live_view(
        {__MODULE__, [:hardware, :updated], []}
      )
      {:reply, new_state, new_state}
    end
  end
end
