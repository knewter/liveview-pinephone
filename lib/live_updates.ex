defmodule LiveUpdates do
  @topic inspect(__MODULE__)
  @pubsub Demo.PubSub

  @doc "subscribe for all users"
  def subscribe_live_view do
    Phoenix.PubSub.subscribe(@pubsub, topic(), link: true)
  end

  @doc "notify for all users"
  def notify_live_view(message) do
    Phoenix.PubSub.broadcast(@pubsub, topic(), message)
  end

  defp topic, do: @topic
end
