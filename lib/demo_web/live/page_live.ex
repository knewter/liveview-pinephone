defmodule LEDComponent do
  use Phoenix.LiveComponent

  def render(assigns) do
    ~L"""
    <button
       class="button <%= unless @led, do: "button-outline" %>"
       phx-click="toggle">Flashlight</button>
    """
  end
end

defmodule DemoWeb.PageLive do
  use DemoWeb, :live_view

  def render(assigns) do
    ~L"""
    <%= live_component(@socket, LEDComponent, led: @hardware.led) %>
    """
  end

  @impl true
  def mount(_params, _session, socket) do
    LiveUpdates.subscribe_live_view()

    {
      :ok,
      socket
      |> assign(
        hardware: Pinephone.Server.get_state()
      )
    }
  end

  @impl true
  def handle_event("toggle", _, socket) do
    new_hardware = Pinephone.Server.toggle_led()
    {:noreply, assign(socket, hardware: new_hardware)}
  end

  def handle_info({_requesting_module, [:hardware, :updated], []}, socket) do
    hardware = Pinephone.Server.get_state()

    {:noreply, assign(socket, hardware: hardware)}
  end
end
